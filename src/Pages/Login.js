import {useEffect, useState, useContext} from "react";
import UserContext from "../UserContext";
import {Button, Form} from "react-bootstrap";
import {Navigate} from "react-router-dom";

export default function Login(){

	//Allowws us to consume the User Context Object and its properties to use for the user validation.
	const {user, 
	} = useContext(UserContext)

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");


	//Check if the values are successfully binded/passed.
	console.log(email);
	console.log(password1);


	//Function to simulate user registration
	function loginUser (e) {
		e.preventDefault();

		//Syntax: localStorage.setItem("propertyName", value);
			//value is from the useState which is the "email"
			// set the email of the authenticated user in the local storage
		localStorage.setItem("email", email);
		setUser({email: localStorage.getItem("email")})

		//Clear input fields
		setEmail ("");
		setPassword1("");
		alert ("Successful Login!")
		// email = "";
		// password1 = "";
		// password2 = "";
	}

	

	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

  useEffect(()=>{
      if(email !== "" && password1 !== ""){
        setIsActive (true);
      }
      else{
        setIsActive(false)
      }
      
  }, [email, password1])
	/*
		Two Way Binding)
			- is a way to assure that we can save the input into our states as we type into the input element.

			Syntax:
				<Form.Control type="inputType" value={inputState} onChange={e => {setInputState(e.target.value)}}

					Note:
						- We set the value of the input type to the initialState. We cannot type into our inputs anymore because there is now a value bound to it.
						- The "onChange" event is added to be able to updated the state bound in the input.

				e = event, contains all the details about the event.

				e.target = target is the element WHERE the event happened.

				e.target.value = the current value of the element WHERE the event happened.
	*/



	return(

		(user.email !== null)
		?
			<Navigate to= "/courses"/>
		:


		<>
			<h1 className="my-5 text-center">User Login</h1>		
			<Form onSubmit = {(e)=> loginUser(e)}>
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
		      </Form.Group>

		      
		      {
		      	isActive
		      	?
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      	:
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      }
		    </Form>
		</>
	)
}
