import Banner from "../Components/Banner";
import Highlights from "../Components/Highlights";

export default function Home () {
    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll"
     }
    
    
    
    return (
        <>
            <Banner data={data}/>
            <Highlights/>
     
        </>
    )
}