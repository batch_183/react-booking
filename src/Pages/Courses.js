import coursesData from "../Data/CoursesData";
import CourseCard from "../Components/CourseCard";
export default function Courses (){
    // console.log(coursesData);
    // console.log(coursesData[0]);

    //Props - is a shorthand for property components are considered as object in ReactJS
            // Props is a way to pass data from parent to child component.
            // It is synonymous to the function parameter.
    
    //map method

    const courses = coursesData.map(course => {
        return (
            <CourseCard key={course.id} courseProp={course}/>
        )
    });
    
    return (       
        <>
            <h1>Courses </h1>    
            {courses}
        </>
    )
}