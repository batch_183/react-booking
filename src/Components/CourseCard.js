import {useState, useEffect} from "react";
import {Card, Button} from "react-bootstrap";


export default function CourseCard ({courseProp}){

//destructure courseProp
const {name, description, price} = courseProp

//States - are used to keep track the information related to individual component
    //syntax  const [stateName, setStateName] = useState(initialStateValue);
                // setStateName - to change the intial state
// Hooks - special react defined methods and functions that allow us to do certain tasks in our components
    //Use the state hook for this component to be able to store its state
  
    //useState () is a hook that creates states
        //useStates () returns an array 2 items:
            // The first itme in the array is the state
            // The second item is the setter function (to chanage the initial state)
 
        //Array destructuring for the useState(), setting intial value
    const [count, setCount] = useState (0);
    // // console.log (useState(0));
    
// Actiivty Answer: 
    // function enroll () {
    //     if (count == 30){
    //         alert ("No more seats")      
    //     }
    //     else {
    //     setCount(count +1);
    //     }

    //     console.log("Enrollees:" + count)
    // }

    // Activity Right Solution:
    const [seats, setSeats] = useState(30);
    const [isOpen, setIsOpen] = useState(false);

		function enroll(){
            setCount(count + 1);
            // console.log("Enrolees: " + count);

            setSeats(seats - 1);
            // console.log("Seats: " + seats);
			
		}

        function unEnroll (){
            setCount(count - 1);
        }

//useEffect - allows us to run a task or an effect, the difference is that that with useEffect we can manipulate when it will run.

// Syntax: 
        //useEffect (function, [dependecy])
            //dependency is optional
        // useEffect(() =>{
        //     if (seats === 0) {
        //         alert ("Nor more seats availabel");
        //         setIsOpen(true);
        //     }
        // }, [seats]);

        // if the useeffect() does not have a dependency array, it will run on the intial render and whenever a state is set by its set fucntion
        // useEffect(() =>{
        //     console.log("use Effect Render")
        // })

        //If useEffect() has a dependency array but empty it will only run on initial render.
        useEffect(() =>{
            console.log("use Effect Render")
        }, [seats])

    return(
       <Card className = "p-3 my-3">
            <Card.Body>
                <Card.Title>
                    Sample Course {`: ${name}`}
                </Card.Title>
                    <Card.Subtitle>
                        Description {`: ${description}`}
                    </Card.Subtitle>
                    <Card.Text>
                        Price      {`: ${price}`}
                    </Card.Text>
                    <Card.Subtitle>
                        Enrolles   {`: ${count}`}
                    </Card.Subtitle>
                    <Card.Text>
                   
                    </Card.Text>
                    <Button variant="primary" onClick={enroll} disabled={isOpen}>Enroll</Button>
                    {/* <Button variant="primary mx-1" onClick={enroll} disabled={isOpen}>Enroll</Button>
                    <Button variant="danger mx-1" onClick={unEnroll} disabled={isOpen}>Unenroll</Button> */}
            </Card.Body>
       </Card>
    )

}