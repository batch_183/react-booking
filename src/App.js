import {useState} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import AppNavbar from "./Components/AppNavbar";

import Courses from "./Pages/Courses";
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import Logout from "./Pages/Logout";
import Register from "./Pages/Register";
import Error from "./Pages/Error";
import {Container} from "react-bootstrap";
import "./App.css";
import {UserProvider, userProvider} from "./UserContext";

function App() {

  // To store the user information and will be used for validating if a user is already logged in on the app or not.
  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })
  console.log(user);

  // Function for clearing localStorage on logout
  const unsetUser = ()=>{
    localStorage.clear();
  }


  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment

    // ReactJS is a single page application (SPA)
      // Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components

    // Router component is used to wrapped around all components which will have access to our routing system.
    
    // We store information in the context by providing teh information using the corresponding "provider" component and passing the information via the "value" prop/attribute.
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
            {/*Routes holds all our Route components.*/}
            <Routes>
            {/*
                - Route assigns an endpoint and displays the appropriate page component for that endpoint.
                - "path" attribute assigns the endpoint.
                - "element" attribute assigns page component to be displayed at the endpoint.
            */}
                <Route exact path ="/" element={<Home />} />
                <Route exact path ="/courses" element={<Courses />} />
                <Route exact path ="/register" element={<Register />} />
                <Route exact path ="/login" element={<Login />} />
                <Route exact path ="/logout" element={<Logout />} />
                <Route exact path ="*" element={<Error />} />
            </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

/*
  Mini Activity

  1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
    - The course name should be in the card title.
    - The description and price should be in the card subtitle.
    - The value for description and price should be in the card text.
    - Add a button for Enroll.
  2. Render the CourseCard component in the Home page.
  3. Take a screenshot of your browser and send it in the batch hangouts

*/

export default App;
